package com.org.greatlearning.assessment.week1;
//here I am declaring my TechDepartment class which extends SuperDepartment class
public class TechDepartment {
//A method to get the Department name
	public String depName() {
		return "Tech Department";}
//A method to getTodaysWork
	public String getTodaysWork() {
		return "Complete coding of module 1";}
//A method to getWorkDeadline
	public String getWorkDeadline() {
		return "Complete by EOD";}
// A method to getTechStackInformation
	public String getTechStackInformation() {
		return "Core Java";}

}
