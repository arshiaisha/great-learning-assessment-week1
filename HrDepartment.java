package com.org.greatlearning.assessment.week1;
//here I am declaring my HRDepartment  class which extends  SuperDepartment class
public class HrDepartment {
//A method to get the Department name
	public String depName() {
		return "HR Department";}
//A method to getTodaysWork
	public String getTodaysWork() {
		return "Fill todays worksheet and mark your attendance";}
//A method to getWorkDeadline
	public String getWorkDeadline() {
		return "Complete by EOD";}
//A method to doActivity
	public String doActivity() {
		return "Team Launch";}
	

}
